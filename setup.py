from setuptools import find_packages, setup

setup(
    name='netbox-zabbix',
    version='0.1.8',
    description='Netbox Plugin - Zabbix',
    long_description='Netbox Plugin - Zabbix',
    url='',
    download_url='',
    author='Michal Drobny',
    author_email='drobny@ics.muni.cz',
    license='Apache 2.0',
    install_requires=[
        'pyzabbix'
    ],
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Framework :: Django",
    ],
    python_requires=">=3.8",
    zip_safe=False,
)