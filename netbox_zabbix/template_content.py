from django.contrib.contenttypes.models import ContentType


from extras.plugins import PluginTemplateExtension
from virtualization.models import VirtualMachine
from dcim.models import Device

from .constants import ZABBIX_API


from .models import ZabbixHost


class ZabbixHostViewDevice(PluginTemplateExtension):
    model = 'dcim.device'

    def right_page(self):
        device = self.context['object']

        zabbix_host = ZabbixHost.objects.filter(
            content_type=ContentType.objects.get_for_model(Device),
            object_id=device.id
        )

        # Do not display the card
        if zabbix_host.count() != 1:
            return ''

        extra_context = {
            'object': zabbix_host.first(),
            'zabbix_server': ZABBIX_API.get('server'),
        }

        return self.render(
            'netbox_zabbix/device_extensions/zabbix_card.html',
            extra_context=extra_context
        )


class ZabbixHostViewVirtualMachine(PluginTemplateExtension):
    model = 'virtualization.virtualmachine'

    def right_page(self):
        virtual_machine = self.context['object']

        zabbix_host = ZabbixHost.objects.filter(
            content_type=ContentType.objects.get_for_model(VirtualMachine),
            object_id=virtual_machine.id
        )

        # Do not display the card
        if zabbix_host.count() != 1:
            return ''

        extra_context = {
            'object': zabbix_host.first(),
            'zabbix_server': ZABBIX_API.get('server'),
        }

        return self.render(
            'netbox_zabbix/device_extensions/zabbix_card.html',
            extra_context=extra_context
        )


template_extensions = [ZabbixHostViewDevice, ZabbixHostViewVirtualMachine]
