from django import template


from dcim.models import Device


from ..constants import ZABBIX_TAG


register = template.Library()


def _check_permission(user, instance, action) -> bool:
    return user.has_perm(
        perm=f'{instance._meta.app_label}.{action}_{instance._meta.model_name}',
        obj=instance
    )


@register.filter()
def can_zabbix(user, device: Device) -> bool:
    return _check_permission(user, device, 'zabbix') \
        and device.tags.filter(name=ZABBIX_TAG).exists()
