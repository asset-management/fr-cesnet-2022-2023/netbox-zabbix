from django import template


register = template.Library()


@register.filter(name='getbadge')
def get_badge(status: str) -> str:
    if status == 'SUCCESS':
        return 'bg-green'
    elif status == 'FAILURE':
        return 'bg-red'
    return 'bg-blue'


@register.filter(name='get')
def get(dictionary, key):
    return dictionary.get(key)
