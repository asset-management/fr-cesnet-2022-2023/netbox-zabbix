import urllib3, json
from typing import Dict, List, Union, Any
from jinja2 import Environment, BaseLoader


from dcim.models import Device


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
OBJECT = Dict[str, Union[str, int]]
OBJECT_ARRAY = List[OBJECT]
ZABBIX_PARAMS = Dict[str, Union[OBJECT, OBJECT_ARRAY]]

CONFIG_CONTEXT = Dict[str, Any]


def get_group(hostgroups, name):
    group = hostgroups.get(name)
    if group is None:
        raise Exception(f'Host Group {name} is not defined.')
    return group.get('id')


def get_template(templates, name):
    template = templates.get(name)
    if template is None:
        raise Exception(f'Template {name} is not defined.')
    return template.get('id')


def empty_template():
    return 'EMPTYTEMPLATE'


class CCParser:
    jinja_env = Environment(loader=BaseLoader)
    jinja_env.globals['get_group'] = get_group
    jinja_env.globals['get_template'] = get_template
    jinja_env.globals['empty_template'] = empty_template

    def __init__(self, device: Device, zabbix_manager) -> None:
        self.device = device
        self.zabbix_manager = zabbix_manager
        self.config_context = self.device.get_config_context()
        self.render_config_context()

    def get_name_resolver(self):
        return self.config_context.get('host')

    def get_config_context(self):
        return self.config_context

    def render_config_context(self) -> None:
        self.jinja_env.globals['hostgroups'] = self.zabbix_manager.host_groups_by_name
        self.jinja_env.globals['templates'] = self.zabbix_manager.templates_by_name
        config_json = json.dumps(self.config_context)
        template = self.jinja_env.from_string(config_json)
        data = template.render(object=self.device)
        self.config_context = json.loads(data)

        if not 'zabbix' in self.config_context:
            raise Exception(f'Device [{self.device.id}]: Cannot find zabbix key in config context data.')
        self.config_context = self.config_context.get('zabbix')
