# Generated by Django 4.1.8 on 2023-06-14 10:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('netbox_zabbix', '0004_remove_zabbixhost_is_synced_zabbixhost_content_type_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='zabbixhost',
            name='netbox_device',
        ),
    ]
