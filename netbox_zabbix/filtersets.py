import django_filters


from netbox.filtersets import BaseFilterSet


class ZabbixHostFilterSet(BaseFilterSet):
    q = django_filters.CharFilter(
        method="search",
        label="Search",
    )

    class Meta:
        fields = ["id", "name"]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset        

        return queryset.filter().distinct()
