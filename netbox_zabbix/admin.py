from django.contrib import admin


from .models import ZabbixLog


@admin.register(ZabbixLog)
class ZabbixLogAdmin(admin.ModelAdmin):
    list_display = ('id', )
