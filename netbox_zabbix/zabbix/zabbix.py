from typing import Optional, Union, Tuple


from pyzabbix import ZabbixAPI


class Zabbix:
    def __init__(
        self, 
        server: str, 
        user: Optional[str] = None,
        password: Optional[str] = None,
        api_token: Optional[str] = None,
        timeout: Optional[Union[float, int, Tuple[int, int]]] = None,
        verify: bool = True,
        cert: Optional[str] = None
    ) -> None:
        self.server = server
        self.user = user
        self.password = password
        self.api_token = api_token
        self.verify = verify
        self.cert = cert
        self.timeout = timeout

        # Must be initialized by as a Context Manager.
        # Example:
        # with Zabbix(...) as zabbix_connector:
        #     ...
        self.client: Optional[ZabbixAPI] = None
        self.data = {} 

    def __enter__(self) -> ZabbixAPI:
        self.client = ZabbixAPI(
            server=self.server,
            timeout=self.timeout,
        ).__enter__()

        if self.verify is False:
            self.client.session.verify = False
        elif self.cert is not None:
            self.client.session.cert = self.cert

        self.client.login(
            user=self.user, 
            password=self.password, 
            api_token=self.api_token
        )

        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.client.__exit__(exception_type, exception_value, traceback)
        return False
