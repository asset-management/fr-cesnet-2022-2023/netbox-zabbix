from typing import Dict


from ..constants import ZABBIX_API
from .zabbix import Zabbix


class ZabbixManager:
    def __init__(self) -> None:

        self.host_groups = HostGroup.collect()
        self.host_groups_by_name = {
            hostgroupdata.get('name'): hostgroupdata for hostgroupdata in self.host_groups.values()
        }

        self.templates = Template.collect()
        self.templates_by_name = {
            templatedata.get('name'): templatedata for templatedata in self.templates.values()
        }

        self.hosts = Host.collect(
            host_groups=self.host_groups,
            templates=self.templates,
        )
        self.hosts_by_name = {
            hostdata.get('host'): hostdata for hostdata in self.hosts.values()
        }

    @staticmethod
    def get_templates_by_name():
        return Template.collect()


class Host:
    @staticmethod
    def collect(*args, host_groups, templates, **kwargs) -> Dict[str, 'Host']:
        objects = {}

        with Zabbix(**ZABBIX_API) as zabbix:
            hosts = zabbix.client.host.get(
                *args,
                selectParentTemplates='extend',
                selectHostGroups='extend',
                selectInterfaces='extend',
                **kwargs
            )
            for host in hosts:
                host_obj = {
                    'id': host.get('hostid'),
                    'host': host.get('host'),
                    'status': host.get('status'),
                    'maintenance_status': host.get('maintenance_status'),
                    'hostgroups': [
                        host_groups.get(hg.get('groupid')) for hg in host.get('hostgroups')
                    ],
                    'templates': [
                        templates.get(t.get('templateid')) for t in host.get('parentTemplates')
                    ],
                    'interfaces': [
                        interface for interface in host.get('interfaces')
                    ]
                }

                objects[host.get('hostid')] = host_obj

        return objects

    @staticmethod
    def get_host_by_id(hostid) -> 'Host':
        with Zabbix(**ZABBIX_API) as zabbix:
            response = zabbix.client.host.get(
                selectParentTemplates='extend',
                selectHostGroups='extend',
                filter={
                    'hostid': hostid,
                }
            )
            if len(response) == 0:
                raise Exception(f'No Zabbix host found (id:{hostid}).')
            return Host(response[0])

    @staticmethod
    def update_host(config) -> None:
        with Zabbix(**ZABBIX_API) as zabbix:
            response = zabbix.client.host.update(**config)
            if len(response) == 0:
                raise Exception(f'Canno create Zabbix host.')

    @staticmethod
    def create_host(config) -> None:
        with Zabbix(**ZABBIX_API) as zabbix:
            response = zabbix.client.host.create(**config)
            if len(response) == 0:
                raise Exception(f'Cannot create Zabbix host.')


class HostGroup:
    @staticmethod
    def collect() -> None:
        objects = {}

        with Zabbix(**ZABBIX_API) as zabbix:
            for hostgroup in zabbix.client.hostgroup.get():
                host_group_obj = {
                    'id': hostgroup.get('groupid'),
                    'name': hostgroup.get('name')
                }
                objects[hostgroup.get('groupid')] = host_group_obj
        
        return objects


class Template:
    @staticmethod
    def collect() -> None:
        objects = {}

        with Zabbix(**ZABBIX_API) as zabbix:
            for template in zabbix.client.template.get():
                template_obj = {
                    'id': template.get('templateid'),
                    'name': template.get('name')
                }
                objects[template.get('templateid')] = template_obj

        return objects
