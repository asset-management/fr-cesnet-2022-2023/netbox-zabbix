import logging


from extras.plugins import get_plugin_config


#
# Logger
#

LOGGER = logging.getLogger(__name__)


#
# Plugin Config
#


ZABBIX_TAG = get_plugin_config('netbox_zabbix', 'zabbix_tag')
ZABBIX_API = get_plugin_config('netbox_zabbix', 'zabbix_api')
