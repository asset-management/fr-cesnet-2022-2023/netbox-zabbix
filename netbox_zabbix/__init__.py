from extras.plugins import PluginConfig


class ZabbixConnector(PluginConfig):
    name = 'netbox_zabbix'
    verbose_name = 'Netbox Plugin - Zabbix'
    description = "Plugin for connecting Netbox to Zabbix."
    version = "0.1.8"
    author = "Michal Drobny"
    author_email = "drobny@ics.muni.cz"
    base_url = "netbox-zabbix"
    min_version = "3.4"
    required_settings = []
    default_settings = {
        'zabbix_tag': 'ZABBIX',
        'zabbix_api': {
            'server': None,
            'password': None,
            'user': None,
            'api_token': None,
            'timeout': 10,
            'cert': None,
            'verify': True
        }
    }


config = ZabbixConnector
