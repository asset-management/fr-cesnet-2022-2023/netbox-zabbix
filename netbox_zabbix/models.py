import traceback
from copy import deepcopy


from django.db import models
from django.urls import reverse
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


from dcim.models import Device
from virtualization.models import VirtualMachine
from utilities.querysets import RestrictedQuerySet
from netbox.models import NetBoxModel


from .constants import ZABBIX_TAG, LOGGER
from .config_context import CCParser
from .zabbix.models import Host

class ZabbixHost(NetBoxModel):
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey(
        'content_type', 
        'object_id'
    )
    zabbix_data = models.JSONField(
        verbose_name="Zabbix Data",
        null=True,
        blank=True,
        default=dict,
    )

    def get_absolute_url(self):
        return reverse("plugins:netbox_zabbix:zabbixhost", args=[self.pk])
    
    def get_cc_data(self):
        return self.content_object.get_config_context().get('zabbix', None)

    def reload(self, zabbix_manager):
        cc_parser = CCParser(self.content_object, zabbix_manager)
        name_resolver = cc_parser.get_name_resolver()
        self.zabbix_data = zabbix_manager.hosts_by_name.get(name_resolver)
        self.save()

    @staticmethod
    def reload_all2(zabbix_manager):
        for netbox_device in Device.objects.filter(
            tags__name=ZABBIX_TAG,
        ):
            if not ZabbixHost.objects.filter(netbox_device__id=netbox_device.id).exists():
                ZabbixHost(netbox_device=netbox_device).save()

        for zabbix_host in ZabbixHost.objects.all():
            zabbix_host.reload(zabbix_manager)

    @staticmethod
    def reload_all(zabbix_manager):
        for netbox_device in Device.objects.filter(
            tags__name=ZABBIX_TAG,
        ):
            if not ZabbixHost.objects.filter(
                content_type=ContentType.objects.get_for_model(Device),
                object_id=netbox_device.id
            ).exists():
                ZabbixHost.objects.create(
                    content_object=netbox_device
                )
        
        for netbox_vm in VirtualMachine.objects.filter(
            tags__name=ZABBIX_TAG,
        ):
            if not ZabbixHost.objects.filter(
                content_type=ContentType.objects.get_for_model(VirtualMachine),
                object_id=netbox_vm.id
            ).exists():
                ZabbixHost.objects.create(
                    content_object=netbox_vm
                )

        for zabbix_host in ZabbixHost.objects.all():
            zabbix_host.reload(zabbix_manager)
    
    def sync(self, zabbix_manager):
        message = None
        try:
            config_context = CCParser(self.content_object, zabbix_manager).get_config_context()
            ZabbixHost.__update_cc(config_context, zabbix_manager)
            if config_context.get('host'):
                Host.create_host(config_context)
            else:
                Host.update_host(config_context)
        except:
            message = traceback.format_exc()
            LOGGER.error(message)
        finally:
            ZabbixLog(
                status='SUCCESS' if not message else 'FAILURE',
                zabbix_host=self,
                request=self.content_object.get_config_context().get('zabbix', {}),
                message=message
            ).save()
    
    @staticmethod
    def __update_cc(config_context, zabbix_manager):
        zabbix_host = zabbix_manager.hosts_by_name.get(config_context['host'])
        
        if zabbix_host:
            config_context['hostid'] = zabbix_host['id']
            config_context.pop('host')

            interface_data = deepcopy(zabbix_host['interfaces'])

            for interface_config in config_context.get('interfaces', []):
                updated = False
                for interface in interface_data:
                    if interface['main'] == '1' and interface['type'] == interface_config.get('type'):
                        interface.update(interface_config)
                        updated = True
                        break
                if not updated:
                    interface_data.append(interface_config)

            config_context['interfaces'] = interface_data

            config_context['templates'] = [template for template in config_context.get('templates', []) if template.get('templateid') != 'EMPTYTEMPLATE']


            config_context['templates_clear'] = []
            for template_config in zabbix_host.get('templates', []):
                if not any(map(lambda x: template_config.get('id') == x.get('templateid'), config_context.get('templates'))):
                    config_context['templates_clear'].append(
                        {
                            'templateid': template_config.get('id'),
                        }
                    )


class ZabbixLog(models.Model):
    datetime = models.DateTimeField(
        auto_now_add=True, 
        editable=False,
        db_index=True,
    )
    status = models.CharField(
        max_length=40, 
        editable=False, 
        default='UNKNOWN'
    )
    zabbix_host = models.ForeignKey(
        to=ZabbixHost,
        on_delete=models.CASCADE,
    )
    request = models.JSONField(
        editable=False,
        verbose_name='Request',
    )
    message = models.TextField(
        verbose_name='Message',
        null=True,
        blank=True,
        editable=False,
    )

    objects = RestrictedQuerySet.as_manager()

    class Meta:
        ordering = ['-datetime']
        verbose_name = 'Zabbix Log'
        verbose_name_plural = 'Zabbix Logs'

    def get_absolute_url(self):
        return reverse('plugins:netbox_zabbix:zabbixlog', args=[self.pk])
