from django.contrib import messages


from netbox.views import generic
from extras.plugins import get_plugin_config


from .constants import ZABBIX_API
from .models import ZabbixLog, ZabbixHost
from .tables import ZabbixLogTable, ZabbixHostTable
from .filtersets import ZabbixHostFilterSet
from .forms import ZabbixHostForm
from .zabbix.models import ZabbixManager


ZABBIX_TAG = get_plugin_config('netbox_zabbix', 'zabbix_tag')


class ZabbixLogListView(generic.ObjectListView):
    queryset = ZabbixLog.objects.all()
    template_name = 'generic/object_list.html'
    table = ZabbixLogTable
    actions = ('export', )


class ZabbixLogView(generic.ObjectView):
    queryset = ZabbixLog.objects.all()


class ZabbixHostListView(generic.ObjectListView):
    queryset = ZabbixHost.objects.all()
    template_name = 'netbox_zabbix/zabbixhost_list.html'
    table = ZabbixHostTable
    actions = ('export', 'bulk_delete', )
    filterset = ZabbixHostFilterSet

    def post(self, request):
        try:
            if '_reload' in request.POST:
                zabbix_manager = ZabbixManager()
                ZabbixHost.reload_all(zabbix_manager)
            elif '_sync' in request.POST:
                zabbix_manager = ZabbixManager()
                for zabbix_host in ZabbixHost.objects.filter(pk__in=request.POST.getlist('pk')):
                    zabbix_host.sync(zabbix_manager)
                zabbix_manager = ZabbixManager()
                for zabbix_host in ZabbixHost.objects.filter(pk__in=request.POST.getlist('pk')):
                    zabbix_host.reload(zabbix_manager)
            elif '_sync_all' in request.POST:
                zabbix_manager = ZabbixManager()
                for zabbix_host in ZabbixHost.objects.all():
                    zabbix_host.sync(zabbix_manager)
                zabbix_manager = ZabbixManager()
                for zabbix_host in ZabbixHost.objects.all():
                    zabbix_host.reload(zabbix_manager)
        except Exception as e:
            messages.error(request, e)
            
        return self.get(request)


class ZabbixHostView(generic.ObjectView):
    queryset = ZabbixHost.objects.all()

    def get_extra_context(self, request, pk):
        return {
            'zabbix_server': ZABBIX_API.get('server'),
        }


class ZabbixHostEditView(generic.ObjectEditView):
    queryset = ZabbixHost.objects.all()
    form = ZabbixHostForm


class ZabbixHostDeleteView(generic.ObjectDeleteView):
    queryset = ZabbixHost.objects.all()


class ZabbixHostBulkDeleteView(generic.BulkDeleteView):
    queryset = ZabbixHost.objects.all()
    filterset = ZabbixHostFilterSet
    table = ZabbixHostTable
