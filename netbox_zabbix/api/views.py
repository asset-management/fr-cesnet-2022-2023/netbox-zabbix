from netbox.api.viewsets import NetBoxModelViewSet


from ..models import ZabbixHost
from ..filtersets import ZabbixHostFilterSet
from .serializers import ZabbixHostSerializer


#
# ZTP templates
#

class ZabbixHostViewSet(NetBoxModelViewSet):
    queryset = ZabbixHost.objects.prefetch_related('data_source', 'data_file')
    serializer_class = ZabbixHostSerializer
    filterset_class = ZabbixHostFilterSet