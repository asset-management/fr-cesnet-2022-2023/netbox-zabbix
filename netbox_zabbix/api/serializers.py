from rest_framework import serializers


from netbox.api.serializers.features import TaggableModelSerializer
from netbox.api.serializers import ValidatedModelSerializer


from ..models import ZabbixHost


#
# ZTP templates
#

class ZabbixHostSerializer(TaggableModelSerializer, ValidatedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='plugins-api:netbox_zabbix-api:zabbixhost-detail')

    class Meta:
        model = ZabbixHost
        fields = '__all__'
