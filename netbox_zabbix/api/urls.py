from netbox.api.routers import NetBoxRouter


from . import views


router = NetBoxRouter()

router.register('zabbix-host', views.ZabbixHostViewSet)

urlpatterns = router.urls
