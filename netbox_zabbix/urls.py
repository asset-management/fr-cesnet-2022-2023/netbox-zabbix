from django.urls import path
from . import views


from .models import ZabbixHost


from netbox.views.generic import ObjectChangeLogView, ObjectJournalView

urlpatterns = [
    ## Zabbix Update Log
    path('zabbix-log/', views.ZabbixLogListView.as_view(), name='zabbixlog_list'),
    path('zabbix-log/<int:pk>/', views.ZabbixLogView.as_view(), name='zabbixlog'),
    
    ## Zabbix
    path('zabbix-host/', views.ZabbixHostListView.as_view(), name='zabbixhost_list'),
    path('zabbix-host/add/', views.ZabbixHostEditView.as_view(), name='zabbixhost_add'),
    path('zabbix-host/delete/', views.ZabbixHostBulkDeleteView.as_view(), name='zabbixhost_bulk_delete'),
    path('zabbix-host/<int:pk>/edit/', views.ZabbixHostEditView.as_view(), name='zabbixhost_edit'),
    path('zabbix-host/<int:pk>/delete/', views.ZabbixHostDeleteView.as_view(), name='zabbixhost_delete'),
    path('zabbix-host/<int:pk>/', views.ZabbixHostView.as_view(), name='zabbixhost'),
    path('zabbix-host/<int:pk>/journal/', ObjectJournalView.as_view(), name='zabbixhost_journal', kwargs={'model': ZabbixHost},),
    path('zabbix-host/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='zabbixhost_changelog', kwargs={'model': ZabbixHost},),
]
