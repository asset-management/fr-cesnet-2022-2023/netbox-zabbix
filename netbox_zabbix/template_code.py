STATUS = """
{% load zabbix_filters %}
<span class="badge {{ record.status|getbadge }}">{{ record.status }}</span>
"""

ID="""
{% load static %}
{% with host=record.zabbix_data.host %}
<a class="btn btn-sm {% if host %}btn-success{% else %}btn-danger{% endif %}" href="{% url 'plugins:netbox_zabbix:zabbixhost' pk=record.pk %}?return_url=/plugins/netbox-zabbix/zabbix-host/" type="button">
    <i class="mdi mdi-eye-outline"></i>
</a>
{% endwith %}
"""


ZABBIX_HOST = """
{% with host=record.zabbix_data.host %}
    {% with hostid=record.zabbix_data.id %}
        {% if host %}
            <span class="badge bg-success">
                <a href="{{ table.zabbix_server }}/zabbix.php?name=&evaltype=0&tags[0][tag]=&tags[0][operator]=0&tags[0][value]=&show_tags=3&tag_name_format=0&tag_priority=&filter_name=&filter_show_counter=0&filter_custom_time=0&sort=name&sortorder=ASC&show_details=0&action=latest.view&hostids[]={{ hostid }}" target="_blank">
                    {{ host }}
                </a>
            </span>
        {% else %}
            —
        {% endif %}
    {% endwith %}
{% endwith %}
"""


HOSTGROUPS = """
{% with hostgroups=record.zabbix_data.hostgroups %}
    {% if hostgroups %}
        {% for hostgroup in hostgroups %}
            <span class="badge bg-blue">
                <a href="{{ table.zabbix_server }}/zabbix.php?action=host.list&filter_set=1&filter_groups[0]={{ hostgroup.id }}" target="_blank">
                    {{ hostgroup.name }}
                </a>
            </span>
        {% endfor %}
    {% else %}
        —
    {% endif %}
{% endwith %}
"""


TEMPLATES = """
{% with templates=record.zabbix_data.templates %}
    {% if templates %}
        {% for template in templates %}
            <span class="badge bg-purple">
                <a href="{{ table.zabbix_server }}/templates.php?form=update&templateid={{ template.id }}" target="_blank">
                    {{ template.name }}
                </a>
            </span>
        {% endfor %}
    {% else %}
        —
    {% endif %}
{% endwith %}
"""


PROBLEMS = """
{% load helpers %}
{% if record.problems is not none %}
    {% for sev_id, severity in table.settings.severities.items %}
        <span class="badge" style="background-color: #{{ severity.color }};">
            <a href="{{ table.zabbix_server }}/zabbix.php?show=1&name=&severities[{{ sev_id }}]={{ sev_id }}&inventory[0][field]=type&inventory[0][value]=&evaltype=0&tags[0][tag]=&tags[0][operator]=0&tags[0][value]=&show_tags=3&tag_name_format=0&tag_priority=&show_opdata=0&show_suppressed=1&show_timeline=1&filter_name=&filter_show_counter=0&filter_custom_time=0&sort=clock&sortorder=DESC&age_state=0&unacknowledged=0&compact_view=0&details=0&highlight_row=0&action=problem.view&hostids[]={{ record.zabbix_host.hostid }}" target="_blank">
                {{ record.problems|get_key:sev_id|length }}
            </a>
        </span>
    {% endfor %}
{% else %}
    —
{% endif %}
"""
