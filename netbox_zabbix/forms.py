from netbox.forms import NetBoxModelForm


from .models import ZabbixHost


class ZabbixHostForm(NetBoxModelForm):
    class Meta:
        model = ZabbixHost
        fields = '__all__'
