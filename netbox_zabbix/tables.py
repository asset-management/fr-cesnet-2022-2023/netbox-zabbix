import django_tables2 as tables


from netbox.tables import columns, NetBoxTable


from .models import ZabbixLog, ZabbixHost
from .template_code import *
from .constants import ZABBIX_API


class ZabbixLogTable(NetBoxTable):
    datetime = tables.DateTimeColumn(
        verbose_name="Date Time",
        format='Y-d-m H:i:s',
    )
    status = tables.TemplateColumn(
        verbose_name='Status',
        template_code=STATUS,
    )
    zabbix_host = tables.Column(
        verbose_name='Netbox Object',
        linkify=True,
    )
    actions = columns.ActionsColumn(
        actions=()
    )
    
    class Meta(NetBoxTable.Meta):
        model = ZabbixLog
        fields = ('pk', 'id', 'datetime', 'status', 'zabbix_host', )
        default_columns = ('pk', 'id', 'datetime', 'status', 'zabbix_host', )


class ZabbixHostTable(NetBoxTable):
    zabbix_server = ZABBIX_API.get('server')
    pk = columns.ToggleColumn(
        visible=True
    )
    id = tables.TemplateColumn(
        linkify=True,
        verbose_name='ID',
        template_code=ID,
    )
    content_object = tables.Column(
        verbose_name='Netbox Object',
        linkify=True,
        orderable=False
    )
    zabbix_host = tables.TemplateColumn(
        verbose_name='Zabbix Host',
        template_code=ZABBIX_HOST,
        orderable=False
    )
    hostgroups = tables.TemplateColumn(
        verbose_name='Host Groups',
        template_code=HOSTGROUPS,
        orderable=False
    )
    templates = tables.TemplateColumn(
        verbose_name='Templates',
        template_code=TEMPLATES,
        orderable=False
    )

    class Meta(NetBoxTable.Meta):
        model = ZabbixHost
        fields = ('pk', 'id', 'content_object', 'zabbix_host', 'hostgroups', 'templates', )
        default_columns = ('pk', 'id', 'content_object', 'zabbix_host', 'hostgroups', 'templates', )
