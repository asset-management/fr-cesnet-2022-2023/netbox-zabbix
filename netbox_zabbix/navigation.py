from extras.plugins import PluginMenuItem, PluginMenu


ZABBIXLOGS_ITEM = PluginMenuItem(
    link='plugins:netbox_zabbix:zabbixlog_list',
    link_text='Zabbix Logs',
    permissions=['netbox_zabbix.view_zabbixlog'],
)


ZABBIXHOSTS_ITEM = PluginMenuItem(
    link='plugins:netbox_zabbix:zabbixhost_list',
    link_text='Zabbix Hosts',
    permissions=['netbox_zabbix.view_zabbixhost'],
)


menu = PluginMenu(
    label='Zabbix',
    groups=(
        (
            'Logs',
            (
                ZABBIXLOGS_ITEM,
                ZABBIXHOSTS_ITEM
            ),
        ),
    ),
    icon_class='mdi mdi-monitor-eye',
)
